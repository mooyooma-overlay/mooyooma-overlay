# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit git-2 cmake-utils

DESCRIPTION="An object-oriented C++ wrapper for cURL tool"
HOMEPAGE="https://github.com/JosephP91/curlcpp"
EGIT_REPO_URI="git://github.com/JosephP91/curlcpp"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND=">=net-misc/curl-7.28.0"

src_install() {
	insinto /usr/include/curlcpp
	doins "${S}/include/*"
	cd "${BUILD_DIR}"
	dolib src/libcurlcpp.a
}
