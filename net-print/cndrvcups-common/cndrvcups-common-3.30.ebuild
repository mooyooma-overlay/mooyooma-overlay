# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5
inherit autotools multilib versionator

DESCRIPTION="Common files for Canon drivers"
HOMEPAGE="http://software.canon-europe.com/software/0046530.asp"
SRC_URI="http://gdlp01.c-wss.com/gds/0/0100005950/03/Linux_UFRIILT_PrinterDriver_V120_uk_EN.tar.gz"

LICENSE="Canon-UFR-II GPL-2 MIT"
SLOT="0"
KEYWORDS="-* amd64 x86"
IUSE=""

# Needed because GPL2 stuff miss their sources in tarball
RESTRICT="mirror"

RDEPEND="
	dev-libs/libxml2
	gnome-base/libglade
	net-print/cups
	x11-libs/gtk+:2
"
DEPEND="${DEPEND}"

S="${WORKDIR}/Linux_UFRIILT_PrinterDriver_V120_uk_EN/Sources/${P}"

# Don't raise a fuss over pre-built binaries
QA_PREBUILT="
	/usr/libexec/cups/filter/c3pldrv
	/usr/$(get_abi_LIBDIR x86)/libColorGear.so.0.0.0
	/usr/$(get_abi_LIBDIR x86)/libColorGearC.so.0.0.0
	/usr/$(get_abi_LIBDIR x86)/libc3pl.so.0.0.1
	/usr/$(get_abi_LIBDIR x86)/libcaepcm.so.1.0
	/usr/$(get_abi_LIBDIR x86)/libcaiousb.so.1.0.0
	/usr/$(get_abi_LIBDIR x86)/libcaiowrap.so.1.0.0
	/usr/$(get_abi_LIBDIR x86)/libcanon_slim.so.1.0.0
	/usr/$(get_libdir)/libcanonc3pl.so.1.0.0
"
QA_SONAME="/usr/$(get_abi_LIBDIR x86)/libcaiousb.so.1.0.0"

S="${WORKDIR}/Linux_UFRIILT_PrinterDriver_V120_uk_EN/Sources/${P}"

change_dir() {
        for i in cngplp buftool backend; do
                cd "${i}"
                "${@}"
                cd "${S}"
        done
}

src_prepare() {
	export "LIBS=-lgmodule-2.0"
	change_dir eautoreconf
}

src_unpack() {
	unpack ${A}
	cd "${WORKDIR}/Linux_UFRIILT_PrinterDriver_V120_uk_EN/Sources/"
	unpack ./${P//}-1.tar.gz
}

src_configure() {
#	emake gen
	change_dir econf
}

src_compile() {
#	sleep 1
	change_dir emake

        # Cannot be moved to 'change_dir' as it doesn't need eautoreconf
        cd "${S}/c3plmod_ipc" && emake

}
