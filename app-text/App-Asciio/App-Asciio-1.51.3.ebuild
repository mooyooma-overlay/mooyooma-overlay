# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

MODULE_AUTHOR=NKH
MODULE_VERSION="${PV}"
inherit perl-module

DESCRIPTION="gtk2-perl application to draw ASCII diagrams"

SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="test"

RDEPEND="dev-perl/Readonly
	dev-perl/Sub-Exporter
	dev-perl/Data-TreeDumper
	dev-perl/glib-perl
	dev-perl/gtk2-perl
	dev-perl/Data-TreeDumper-Renderer-GTK
	dev-perl/Compress-Bzip2
	dev-perl/Eval-Context
	virtual/perl-File-Spec
	dev-perl/List-MoreUtils
	virtual/perl-Scalar-List-Utils
	virtual/perl-MIME-Base64
	dev-perl/File-Slurp
	dev-perl/Algorithm-Diff
	dev-perl/Clone
	dev-perl/Module-Util
	"

#Directory::Scratch::Structured
#Hash::Slice
DEPEND="${RDEPEND}
	dev-perl/Module-Build
	test? (
		dev-perl/Test-NoWarnings
		dev-perl/Test-Warn
	)
"

SRC_TEST="do"
