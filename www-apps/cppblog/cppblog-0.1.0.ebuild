# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
SLOT="0"
KEYWORDS="~amd64 ~x86"

DESCRIPTION="CppBlog is a high performance blog engine based on CppCMS technology."
HOMEPAGE="http://cppcms.com"
LICENSE="GPL-3"

inherit cmake-utils

SRC_URI="mirror://sourceforge/cppcms/${PN}_${PV}.tar.bz2"

RDEPEND=">=media-gfx/imagemagick-6.8.9.9[fontconfig,png,truetype]"

DEPEND=">=app-text/discount-2.1.6-r1
	|| ( >=dev-cpp/cppcms-1.0.3 dev-cpp/cppcms-ng )
	>=dev-db/cppdb-0.3.1-r1"

S=${WORKDIR}/${PN}_${PV}
