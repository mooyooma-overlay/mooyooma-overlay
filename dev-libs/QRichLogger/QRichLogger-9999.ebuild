# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$
EAPI=6

inherit cmake-utils

if [ ${PV} == "9999" ]; then
        inherit git-r3
        EGIT_REPO_URI="https://gitlab.com/anonymous0/${PN}.git"
else
        inherit vcs-snapshot
        SRC_URI="https://gitlab.com/anonymous0/${PN}/repository/archive.tar.bz2?ref=${PV} -> ${P}.tar.bz2"
        KEYWORDS="~x86 ~amd64"
fi


DESCRIPTION="Qt-based logger, optimized for GUI-applications."
HOMEPAGE="https://gitlab.com/anonymous0/QRichLogger"
LICENSE="GPLv3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
