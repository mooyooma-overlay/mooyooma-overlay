# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
SLOT="0"

inherit git-2

DESCRIPTION="Pure-C HTML5 parser"
HOMEPAGE="https://github.com/google/gumbo-parser"
LICENSE="Apache-2.0"
KEYWORDS="~amd64 ~x86"
IUSE="doc static-libs"

DEPEND="
	doc? ( app-doc/doxygen )"

EGIT_BRANCH="master"
EGIT_REPO_URI="git://github.com/google/gumbo-parser.git"

S=${WORKDIR}

src_configure()
{
	bash autogen.sh
	econf
}

src_compile() {
	default
	use doc && doxygen
}

src_install() {
	emake DESTDIR="${D}" LIBDIR="$(get_libdir)" PREFIX="${EPREFIX}/usr" LIBTOOL="./libtool" install
	use static-libs || find "${ED}" -name 'lib*.la' -delete
	use doc && dohtml -r "${S}"/docs/html/*
}
