# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

inherit cmake-utils git-r3

DESCRIPTION="This library implements Kaitai Struct API for C++ using STL."
HOMEPAGE="http://kaitai.io/"

EGIT_REPO_URI="https://github.com/CarelessChaser/kaitai_struct_cpp_stl_runtime.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~x86 ~amd64"
