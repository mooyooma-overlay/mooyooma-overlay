# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header

EAPI=5

MODULE_AUTHOR=NKH
MODULE_VERSION=${PV}
inherit perl-module

DESCRIPTION="Evalute perl code in context wraper"

SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~ia64 ~ppc ~ppc64 ~sparc ~x86"
IUSE="test"

RDEPEND="dev-perl/Readonly
	dev-perl/File-Slurp
	dev-perl/Sub-Install
	virtual/perl-Safe
	virtual/perl-Data-Dumper"

DEPEND="${RDEPEND}
	dev-perl/Module-Build
	test? (
		dev-perl/Data-TreeDumper
		dev-perl/Text-Block
		dev-perl/Test-Exception
		dev-perl/Test-NoWarnings
		dev-perl/Test-Warn
		dev-perl/Directory-Scratch-Structured
		dev-perl/Test-Output
	)"

SRC_TEST=do
